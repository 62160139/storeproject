/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.storeproject;

import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class TestDelete {
     public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "DELETE FROM PRODUCT WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 5);
            int row = stmt.executeUpdate();

            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelect.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
